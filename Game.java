//Main method class to construct the Events, Character, Score classes
//Test class using the MainMethodTest class, create new instances of the classes

public class Game {
	
	public Event events;
	public Character character;
	
	//Constructor for classes
	public Game(Character startCharacter,Event startEvents) {
		events = startEvents;
		character = startCharacter;
	}
	
	public static void startGame(Character hero, Event events)
	{
	    events.gameRoot(hero);
	}
	
	//Methods
    public void setEvents(Event newEvents) {
        events = newEvents;
    }
        
    public void setCharacter(Character newCharacter) {
        character = newCharacter;
    }
    
    public static void main(String[] args)
    {
    	Character peasantJoe = new Character();
		Event theEvents = new Event();
    	Game theGame = new Game(peasantJoe, theEvents);
    	startGame(peasantJoe, theEvents);
    	
    } 
        
}
