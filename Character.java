import java.util.Scanner;

/**
  *Rough first attempt at the Character class, which is one that holds the necessary information for the player's character, and
  *includes methods corresponding to the various changes that can occur to that information
  */

public class Character {

  private String name;
  private int health = 100;
  private int attackLevel=5;
  private int intoxication=0;
  
  public int getHealth () {
    
    return health;
    }
    
  
/**
  *name is a mutator method that gives the character it's name by taking input from the user.
  */
  
  public void name() {
  
    Scanner keyIn = new Scanner(System.in);
    System.out.println("Please enter the name of your character:");
    name = keyIn.nextLine();
    }

/**
  *takesDamage is a mutator method that changes the Character object's health by an amount
  *passed to the method, after being hurt by something other than another character.
  */
    
  public void takesDamage (int damage) {
  
    health = health - damage;
    }
    
/**
  *restoreHealth is a mutator method which changes the health of the Character object by 
  *an amount passed to the method, after the character restores health.
  */
    
  public void restoreHealth (int amount_of_health) {
  
    health = health + amount_of_health;
    }
    
/** 
  *attacks is a character that is run on a Character object that is attacking the 
  *Character objects that the method is passed, the method is also passed an integer 
  *representing the power of the Character's attack
  */
  
  public void attacks (Character attacked, int power_of_attack) {
    
    attacked.health = attacked.health - (power_of_attack * attackLevel);
    }
    
/**
  *gainAttack is a mutator class that is passed an integer representing
  *the level of attack gained by a Character object and changes its attackLevel accordingly
  */
 
  public void gainAttack (int attackGained) {
    
    attackLevel = attackLevel + attackGained;
    }
    
/**intoxicated is a mutator method that increases or decreases a player's intoxication
  *by the amount passed to the method
  */

     
  public void intoxicated (int intoxicationLevel) {
  
    intoxication = intoxication + intoxicationLevel;
    }
    
/**print is an accessor method that displays the character's current statistics
	*/
	
  public void print() {
  
    System.out.print("Character name: " + name + "\nCharacter health: " + health + "\nCharacter attack power: " + attackLevel + "\nCharacter intoxication: " + intoxication +"\n");
    }
  }
  
    