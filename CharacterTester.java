public class CharacterTest { 

	public static void main(String args[]) {
	
		
		Character player1 = new Character();//Creates new Character Object
		player1.name();//Tests the name method
		player1.print();//Tests the print method(Expected output:Character name: given name Character health: 100 Character attack Level: 5 Character intoxication = 0)
		player1.takesDamage(2);//Tests the takesDamage method
		player1.print();//Ensures Takes damage functioned correctly(Expected health output: 98)
		player1.restoreHealth(3);//Tests restoreHealth method
		player1.print(); //Ensures Takes damage worked(expected health output: 101)
		Character enemy = new Character();
		enemy.attacks(player1,2);//Tests the attacks method
		player1.print(); //Ensures the attacks method correctly functioned (Expected health output: 91)
		player1.gainAttack(3); //Tests the gainAttack method 
		player1.print();//Ensures the gainAttack method worked (Expected attack level output: 8)
		player1.intoxicated(3);//Tests the intoxicated method
		player1.print();//Ensures the intoxicated method functioned correctly(expected intoxication output:3)
		}
	}
		