import java.util.Scanner;

public class MasterTest 
{
    /*
     *  charTest will initilize a character adn test the methods involved
     *  with the character including mostly removing and adding character
     *  stats. 
     */
    public static void charTest()
    {
    	Character joe = new Character();
	System.out.println("This is for character testing, and the initilizing of character stats including health at 100 : " + joe.getHealth() + " and attack at 1 : " + joe.getAttack()+ " and finaly a the name should be the same: " + joe.getName());
	joe.takes_damage(20);
	if(joe.getHealth() != 80)
	    {
		System.out.println("After taking 20 damage, where the health should be 80 the result was rather: "+joe.getHealth());
	    }
	joe.restore_health(20);
	if(joe.getHealth() != 100)
	    {
		System.out.println("After restoring 20 health from 80 health, where the health should be 100 the result was rather: "+joe.getHealth());
	    }
	joe.gain_attack(1);
	if(joe.getAttack() != 2)
	    {
		System.out.println("After the gain of 1 point in attack, attack should eb at 2, the result was rather: "+joe.getHealth());
	    }

    }		
    /*
     * eventTest will make a run through a short series of events that 
     * are currently implimented. 
     */
    public static void eventTest()
    {
	 Character peasantJoe = new Character();
	 Event theGame = new Event();
	 theGame.gameRoot(peasantJoe);
    }

    /*
     * Test game, that really only calls what event and character do, 
     * if not less. 
     */
    public static void gameTest()
    {
      Character c1 = new Character();
      Event e1 = new Event();
      Game g1 = new Game(c1,e1);
      System.out.printf("Created events: %s\n", e1);
    }
    /*
     * scoreTest will create a score, a bonus, and finaly a timed bonus 
     * and make alterations to them accordingly. 
     */
    public static void scoreTest()
    {	
	Scanner scan = new Scanner(System.in);
        Score s1 = new Score();
        s1.setScore(100);
        if(s1.getScore() != 100)
	    {
		System.out.println("Something is wrong with set score. The result of setting to 100 was: " + s1.getScore());
	    }
        s1.setBonus(200);
	if(s1.getBonus() != 200)
	    {
		System.out.println("Setting bonus to 200 resulted in: "+ s1.getBonus());
	    }
        s1.changeScore(-50);
	if(s1.getScore() != 50)
	    {
		System.out.println("There is an error. Changing score by -50 resulted in: "+s1.getScore());
	    }
        s1.changeBonus(50);
	if(s1.getBonus() != 250)
	    {
		System.out.println("There is an error. Changing the bonus by +50 resulted in: "+s1.getBonus());
	    }
        s1.addBonus();
	if(s1.getScore() != 300)
	    {
		System.out.println("There is an error. Addding the bonus to the score resulted in: "+ s1.getScore());
	    }
        long temp = s1.timeBonus(10000);
	System.out.println("The next test is time dependant, press enter to stop the watch.");
        String s = scan.nextLine();
        long temp2 = s1.endTime();
        System.out.println("The start time was "+temp + " nanoseconds and the end time was "+temp2+ " nanoseconds and the final bonus is " + s1.getBonus());
    }
    public static void main(String[] args)
    {
	charTest();
	eventTest();
	scoreTest();
    }
}