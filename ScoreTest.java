import java.util.Scanner;

public class ScoreTest
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        Score s1 = new Score();
        s1.setScore(100);
        System.out.println(s1.getScore());
        s1.setBonus(200);
        System.out.println(s1.getBonus());
        s1.changeScore(-50);
        System.out.println(s1.getScore());
        s1.changeBonus(50);
        System.out.println(s1.getBonus());
        s1.addBonus();
        System.out.println(s1.getScore());
        s1.timeBonus(10000);
        String s = scan.nextLine();
        s1.endTime();
        System.out.println(s1.getBonus());

    }
}