# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
#	Class Test Instructions -  How to test the Classes
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# ! How to test Character.java
#
#		Find 'CharacterTester.java'
#		Compile 'CharacterTester.java'
#
#		Expected Results: 
#			"Please enter the name of your character:" -> User Input. 
#			User Input for Character Name
#			"100"
#			"1"
#			"Please enter the name of your character:" -> User Input. 
#			User Input for Character Name
#			"100"
#			"1"

# ! How to test Score.java
#
#		Find 'ScoreTest.java' in the Code Repository
#		Compile 'ScoreTest.java'.
#
#		Expected Results:
#			100
#			200
#			50
#			250
#			300

# ! How to test Event.java
#
#		Find 'EventTester.java' in the Code Repository
#		Compile 'Event Tester.java'.
#
#		Expected Results:
#			"Please enter the name of your character:" -> User Input for name
#			"Some odd code will be executed for this event."
#			"Select your next branch. Enter the word either red or blue. No caps" -> User Input. Enter either red or blue.
#				if "red" is chosen: "You are now in the red branch of the game."
#				if "blue" is chosen: "You are now in the blue branch of the game."
#			"But this code executes still, which indicates we may be stacking things..."


# ! How to test Game.java
#
#	Find 'GameTest.java' in the code repository
#	Compile 'GameTest.java'
#
#	Expected results: class instances created for Events, Character, Score
#		"Created events."
#		"Created character."