import java.util.Scanner;

/**
 * Work in progress Game Class. As the plot is laid out more definitively, then this will be altered.
 */

public class Event
{
	/**	
	 *	The root of all games. Only public method that will guide the player through the branches.
	 *
	 *	@param: Character object for this game's hero.
	 *	@return: none.
	 */
	public void gameRoot(Character hero)
	{
		System.out.println("Welcome to the game," + hero.getName());
		System.out.println("Some odd code will be executed for this event.");
		System.out.println("Select your next branch. Enter the word either red or blue. No caps");
		
		Scanner keyin = new Scanner(System.in);
        String branchChoice = keyin.nextLine();
		
		if(branchChoice.equals("red"))
		{
			redBranch(hero);
		}else if(branchChoice.equals("blue"))
		{
			blueBranch(hero);
		}else
		{
			System.out.println("Invalid Input");
		}
		
		System.out.println("But this code executes still, which indicates we may be stacking things...");
			
	}
	
	/**
	 *	Example redBranch. The code within this branch will be executed if the redBranch is selected.
	 *
	 *	@param: Character object for this branch's hero.
	 *	@return: none.
	 */
	private static void redBranch(Character hero)
	{
		System.out.println("You are now in the red branch of the game.");
	}
	
	/**
	 *	Example blueBranch. The code within this branch will be executed if the blueBranch is selected.
	 *
	 *	@param: Character object for this branch's hero.
	 *	@return: none.
	 */
	private static void blueBranch(Character hero)
	{
		System.out.println("You are now in the blue branch of the game.");
	}
}