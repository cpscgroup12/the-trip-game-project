# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
#	Group 12
#
#	Team Members: Sebastian Cretes, Kevin Reyes, Richard Pham, Rik Mantel
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#	THIRD DELIVERABLE INFORMATION BELOW
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# ! Original Design Document Location: Code Repository on BitBucket
# ! Code Repository: Bit Bucket (https://bitbucket.org/cpscgroup12)

# ! Third Deliverable Details:
# 		Design Document for this deliverable is titled "Third_DesignDoc.pdf" found in the Code Repository.
# 		A new class for testing all classes is called MasterTest.java. Please download this, include it 
#		in the same file as the others, and run it in order to test all files.
#
#		Due to conflicts in schedule and time availability, we do not have a working version of the game for this deliverable. 

# ! How to Test Code:
#		Download the needed files:
#			Game.java, Event.java, Character.java, Score.java
#		Ensure they are within the same file.
#		Compile Game.java. As the main class, this will run through our current framework of the game.
#		
#		Please refer to "ClassTest_Instructions" for details on how to test the overall Game.



#	SECOND DELIVERABLE INFORMATION BELOW
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# ! Original Design Document Location: Code Repository on BitBucket
# ! Code Repository: Bit Bucket (https://bitbucket.org/cpscgroup12)

# ! Second Deliverable Details:
# 		Design Document for this deliverable is titled "Second_DesignDoc.pdf" found in the Code Repository.
# 		Class Tests Document is titled "ClassTest_Instructions.md", found in the Code Repository. 
#			This document will elaborate on how to test each class and expected results.

# ! How to Test Code:
#		Download the needed files:
#			Game.java, Event.java, Character.java, Score.java
#		Ensure they are within the same file.
#		Compile Game.java. As the main class, this will run through our current framework of the game.
#		
#		Please refer to "ClassTest_Instructions" for details on how to test each class individually.

# ! Included Classes:
#		Character Class
#		CharacterTester Class
#		
#		Event Class
#		EventTester Class
#
#		Score Class
#		ScoreTest Class
#
#		Game Class
#		GameTest Class