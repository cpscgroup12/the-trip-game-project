import java.lang.Math;

public class Score
{
    private int score = 0;
    private int bonus = 0;
    // b stands for bonus.
    private long startB;
    private long endB;
    private long diffTime;
    // int score = 0;
    // int bonus = 0;
    // b stands for bonus.
    // long startB;
    // long endB;
    // long diffTime;

    /**
     * Changes the score by adding a set value.
     * mutator 
     * @param int  
     */
    public void changeScore(int value)
    {
	score = score + value;
	if(score < 0)
	    {
		score = 0;
	    }
    }
    /**
     * Sets the score to a fixed value.
     * mutator
     * @param int value
     */
    public void setScore(int value)
    {
	score = value;
    }

    /**
     * Gets and returns the score.
     * @return score
     */
    public int getScore()
    {
	return this.score;
    }
    /**
     * Gets and returns the bonus.
     * @return bonus
     */
    public int getBonus()
    {
	return this.bonus;
    }
    
    /**
     * Sets the bonus to a set value.
     * mutator
     * @param int value
     */

    public void setBonus(int value)
    {
	bonus = value;
    }
    /**
     * Changes the bonus by adding a set value.
     * mutator 
     * @param int  
     */
    public void changeBonus(int value)
    {
	bonus = bonus + value;
	if( bonus < 0)
	    {
		bonus = 0;
	    }
    }
    /**
     * Sets a bonus and gets a time reading.
     * mutator
     * @param value
     */
    public long timeBonus(int value)
    {
	bonus= value;
	startB = System.nanoTime();
	return startB;
    }
    /**
     * Changes the bonus based on the diffirence of current time.
     * mutator
     */
    public long endTime()
    {
	endB = System.nanoTime();
	diffTime = endB - startB;
	diffTime = diffTime/(int)(Math.pow(10,9));
	if( bonus > (20*(int)diffTime))
	    {
		bonus = bonus - (int)(20 * (int)diffTime);
	    }

	else
	    {
		bonus = 0;
		    }
	return endB;
    }
    /**
     * Adds the bonus to the score
     * mutator
     */
    public void addBonus()
    {
	score = score + bonus;
    }
}